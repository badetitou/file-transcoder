package com.io.encoding;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collection;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.NameFileFilter;

/**
 * Utility class for batch transcoding files from one encoding to another.
 * 
 * @author Gabriel DARBORD
 */
public class FileTranscoder {
	private static boolean verbose = false;

	private FileTranscoder() {
	}

	/**
	 * Entry point to use this class as a standalone.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		verbose = true;

		// define parameters
		Option dirsOption = Option.builder("dirs").hasArgs().argName("directories...")
				.desc("directories where to find files to transcode").required().build();
		Option srcOption = Option.builder("src").hasArg().argName("source encoding")
				.desc("original encoding of the source files").required().build();
		Option tgtOption = Option.builder("tgt").hasArg().argName("target encoding")
				.desc("encoding to which the files will be transcoded").required().build();
		Option filterOption = Option.builder("filter").hasArg().argName("filename")
				.desc("only transcode files with the given name").build();

		Options options = new Options();
		options.addOption(dirsOption);
		options.addOption(srcOption);
		options.addOption(tgtOption);
		options.addOption(filterOption);

		// parse arguments
		CommandLine cmd = null;
		try {
			CommandLineParser parser = new DefaultParser();
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			HelpFormatter help = new HelpFormatter();
			help.printHelp("java -jar file-transcoder-0.0.1.jar", options, true);
			System.out.println(
					"For supported encodings and their canonical name, refer to: https://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.html");
			System.exit(1);
		}

		String[] dirNames = cmd.getOptionValues(dirsOption);
		String srcEncoding = cmd.getOptionValue(srcOption);
		String tgtEncoding = cmd.getOptionValue(tgtOption);

		IOFileFilter filter;
		if (cmd.hasOption(filterOption)) {
			String filterValue = cmd.getOptionValue(filterOption);
			filter = new NameFileFilter(filterValue);
		} else {
			filter = FileFileFilter.INSTANCE;
		}

		try {
			transcode(dirNames, srcEncoding, tgtEncoding, filter);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("All done!");
		verbose = false;
	}

	/**
	 * Iterate recursively over the files in the directories specified by
	 * {@code dirNames} and transcode them in place from {@code srcEncoding} to
	 * {@code tgtENcoding}.
	 * 
	 * @param dirNames    directories to iterate over
	 * @param srcEncoding encoding of the source files
	 * @param tgtEncoding new encoding for the files
	 * @throws IOException
	 */
	public static void transcode(String[] dirNames, String srcEncoding, String tgtEncoding) throws IOException {
		transcode(dirNames, srcEncoding, tgtEncoding, FileFileFilter.INSTANCE);
	}

	/**
	 * Iterate recursively over the files in the directories specified by
	 * {@code dirNames} and transcode them in place from {@code srcEncoding} to
	 * {@code tgtENcoding}. Only files matching the {@code filter} will be affected.
	 * 
	 * @param dirNames    directories to iterate over
	 * @param srcEncoding encoding of the source files
	 * @param tgtEncoding new encoding for the files
	 * @param filter      restrict files to be transcoded
	 * @throws IOException
	 */
	public static void transcode(String[] dirNames, String srcEncoding, String tgtEncoding, IOFileFilter filter)
			throws IOException {
		for (String dirName : dirNames) {
			if (verbose) {
				System.out.println("Searching files in `" + dirName + "'...");
			}

			File dir = new File(dirName);
			Collection<File> files = FileUtils.listFiles(dir, filter, DirectoryFileFilter.DIRECTORY);

			if (verbose) {
				System.out.println("Transcoding " + files.size() + " files...");
			}

			for (File file : files) {
				File tmp = File.createTempFile("tmp", "");

				try {
					transcode(file, srcEncoding, tmp, tgtEncoding);
				} catch (IOException e) {
					tmp.delete();
					throw e;
				}

				Files.move(tmp.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
		}
	}

	/**
	 * Read the {@code source} file using the encoding specified by
	 * {@code srcEncoding}, and writes its content to the {@code target} file with
	 * the encoding specified by {@code tgtEncoding}. No checks are made to verify
	 * that {@code source} is encoded with {@code srcEncoding}, it is the caller's
	 * responsibility.
	 * 
	 * @param source      file to be transcoded
	 * @param srcEncoding encoding of the source file
	 * @param target      file that will contain the transcoded content of the
	 *                    source file
	 * @param tgtEncoding encoding of the target file
	 * @throws IOException
	 */
	public static void transcode(File source, String srcEncoding, File target, String tgtEncoding) throws IOException {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(source), srcEncoding));
			 BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(target), tgtEncoding))) {
			char[] buffer = new char[16384];
			int read;
			while ((read = br.read(buffer)) != -1)
				bw.write(buffer, 0, read);
		}
	}

}
